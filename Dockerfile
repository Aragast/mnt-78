FROM centos:7

RUN yum update -y && yum install -y python3 python3-pip
RUN pip3 install flask flask_restful flask_jsonpify

ADD python-api.py /opt/python-api.py

ENTRYPOINT ["python3", "/opt/python-api.py"]
